# ! bin/bash

#block Whatsapp
apt update
echo "127.0.0.1		web.whatsapp.com" >> /etc/hosts
echo "127.0.0.1         www.whatsapp.com" >> /etc/hosts
echo "127.0.0.1         www.facebook.com" >> /etc/hosts
echo "127.0.0.1         www.instagram.com" >> /etc/hosts
echo "127.0.0.1         www.youtube.com" >> /etc/hosts

apt update && apt upgrade

#add Repository
echo "deb http://old-releases.ubuntu.com/ubuntu/ raring main universe restricted multiverse" >> /etc/apt/sources.list
echo "deb-src http://old-releases.ubuntu.com/ubuntu/ raring main universe restricted multiverse" >> /etc/apt/sources.list
echo "deb http://old-releases.ubuntu.com/ubuntu/ raring-security main universe restricted multiverse" >> /etc/apt/sources.list
echo "deb-src http://old-releases.ubuntu.com/ubuntu/ raring-security main universe restricted multiverse" >> /etc/apt/sources.list
echo "deb http://old-releases.ubuntu.com/ubuntu/ raring-updates main universe restricted multiverse" >> /etc/apt/sources.list
echo "deb-src http://old-releases.ubuntu.com/ubuntu/ raring-updates main universe restricted multiverse" >> /etc/apt/soutces.list
echo "deb http://old-releases.ubuntu.com/ubuntu/ raring-backports main restricted universe multiverse" >> /etc/apt/sources.list
echo "deb-src http://old-releases.ubuntu.com/ubuntu/ raring-backports main restricted universe multiverse" >> /etc/apt/sources.list
echo "deb http://old-releases.ubuntu.com/ubuntu/ raring-proposed main restricted universe multiverse" >> /etc/apt/sources.list
echo "deb-src http://old-releases.ubuntu.com/ubuntu/ raring-proposed main restricted universe multiverse" >> /etc/apt/sources.list

apt update

apt-get install -y default-jdk
apt-get install -y gnome-panel
apt-get install -y xarchiver

#install VPN
apt install network-manager-openvpn-gnome

apt update && apt upgrade

# install chrome
apt install -f -y
sudo apt-get install libxss1 libappindicator1 libindicator7 -y
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo dpkg -i google-chrome*.deb

apt update && apt upgrade

# block usb
chmod 700 /media




